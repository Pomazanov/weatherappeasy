package com.fullspeed17.weathereasy.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.fullspeed17.weathereasy.R
import com.fullspeed17.weathereasy.databinding.FragmentMainBinding
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.math.roundToInt

const val API_key = "dc91ae199f4dbcf5e76e92b3afbd6ace"

class MainFragment : Fragment() {
    lateinit var bindingClass: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        bindingClass = FragmentMainBinding.inflate(inflater,container, false)
        return bindingClass.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }

    private fun init(){
        bindingClass.btFind.setOnClickListener {
            val nameCity = bindingClass.edNameCity.text.toString().trimEnd()
            if (nameCity.isNotEmpty()) {
                getResult(nameCity)
            } else {
                Toast.makeText(context, getString(R.string.empty_name_city),Toast.LENGTH_LONG).show()
            }
        }
    }


    private fun getResult(name: String) = with(bindingClass){
        val url = "https://api.openweathermap.org/data/2.5/weather?q=" +
        name.lowercase(Locale.getDefault()) +
        "&appid=" + API_key + "&lang=ru"
        val queue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(Request.Method.GET,
        url,
            { response ->
                val jsonObject = JSONObject(response)

                val temp = jsonObject.getJSONObject("main").getString("temp")
                //val nameCity = jsonObject.getString("name")
                val timeZone = jsonObject.getString("timezone")
                val description = (jsonObject.getJSONArray("weather")[0] as JSONObject)
                    .getString("description")
                val iconId = (jsonObject.getJSONArray("weather")[0] as JSONObject)
                    .getString("icon")
                val urlIcon = "http://openweathermap.org/img/wn/" + iconId + "@2x.png"

                tvResult.text = conversionInCelsius(temp) + getString(R.string.name_C) + " "
                //tvNameCity.text = "В " + nameCity.uppercase()
                tvDescription.text = description
                imIconWeather.visibility = View.VISIBLE
                Picasso.get()
                    .load(urlIcon)
                    .into(imIconWeather)
                tvTimeZone.text = "ВРЕМЕННАЯ ЗОНА: " + (timeZone.toInt() / 3600).toString()

                Log.d("MyLog", "Result: $response")
            },
            {
                Toast.makeText(context,getString(R.string.error),Toast.LENGTH_SHORT).show()
            }
        )
        queue.add(stringRequest)
    }

    private fun conversionInCelsius (temp: String): String {
        val conversion = (temp.toFloat() - 273.15)
        val conversionTemp = " " + conversion.roundToInt().toString()
        //val conversionTemp = String.format("%.1f",conversion)
        return conversionTemp
    }

    private fun checkSign (variable: String) {
        if (variable.toInt() >= 0){

        }
    }



    companion object {
        @JvmStatic
        fun newInstance() = MainFragment()
    }
}