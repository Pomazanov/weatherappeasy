package com.fullspeed17.weathereasy

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fullspeed17.weathereasy.databinding.ActivityMainBinding
import com.fullspeed17.weathereasy.fragments.MainFragment

class MainActivity : AppCompatActivity() {

    lateinit var bindingClass:ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingClass = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bindingClass.root)
        supportFragmentManager
            .beginTransaction().replace(R.id.placeHolder, MainFragment.newInstance())
            .commit()
    }
}